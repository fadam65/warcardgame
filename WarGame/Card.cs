﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    public class Card
    {
        private int rank;
        private int suit;

        private static string[] suits = new string[] { "Clubs", "Diamonds", "Spades", "Hearts" };
        private static string[] ranks = new string[] {"2", "3", "4", "5", "6", "7", "8", "9", "10",
                                                      "Jack", "Queen", "King", "Ace" };
        public static int CLUBS     = 0;
        public static int DIAMONDS  = 1;
        public static int SPADES    = 2;
        public static int HEARTS    = 3;

        public static int JACK  = 11;
        public static int QUEEN = 12;
        public static int KING  = 13;
        public static int ACE   = 14;

       /**************************
       * Constructor  -blank 
       *
       ***************************/
        public Card()
        {
            this.rank = 0;
            this.suit = 0;
        }

        /****************************************
         * Copy Constructor
         * PARAMETER    -rank: value for the rank (2-14)
         * PARAMETER    -suit: value for the suit (0-3)
         * RETURN       -n/a
         ***************************************/
        public Card (Card OtherCard)
        {
            this.rank = OtherCard.rank;
            this.suit = OtherCard.suit;
        }

        /****************************************
         * Constructor
         * PARAMETER    -rank: value for the rank (2-14)
         * PARAMETER    -suit: value for the suit (0-3)
         * RETURN       -n/a
         ***************************************/
        public Card (int rank, int suit)
        {
            if ((rank < 0) || (rank > 12))
                throw new ArgumentOutOfRangeException("Rank of card is not within valid range (2-14): " + rank);
            else
                this.rank = rank;
            if ((suit < 0) || (suit > 3))
                throw new ArgumentOutOfRangeException("Suit of card is not within valid range (0-3): " + suit);
            else
                this.suit = suit;
        }

        /****************************************
         * GetRank      -returns the card's rank.
         * PARAMETER    -n/a
         * RETURN       -int: rank
         ***************************************/
        public int GetRank() { return this.rank; }

        /****************************************
         * GetSuit      -returns the card's suit.
         * PARAMETER    -n/a
         * RETURN       -int: suit
         ***************************************/
        public int GetSuit() { return this.suit; }

        /****************************************
         * SetRank      -sets the card's rank.
         * PARAMETER    -the new value for rank.
         * RETURN       -n/a
         ***************************************/
        public void SetRank(int rank) { this.rank = rank; }

        /****************************************
         * SetSuit      -sets the card's suit.
         * PARAMETER    -the new value for suit.
         * RETURN       -n/a
         ***************************************/
        public void SetSuit(int suit) { this.suit = suit; }

        /****************************************
         * ConvertToString  -returns the card as a string
         * PARAMETER        -n/a
         * RETURN           -string: the card's description
         ***************************************/
         public string ConvertToString()
        {
            return ( ranks[this.rank] + " of " + suits[this.suit] );
        }

        /****************************************
         * CompareRank  -Compares this Card with another Card
         * PARAMETER    -another Card object, c
         * RETURN       -int: 0 if ranks are equal.
         *                    neg(-) if this rank is lower.
         *                    pos(+) if this rank is higher.
         ***************************************/
         public int CompareRank(Card c)
        {
            return (this.rank - c.rank);
        }

    }
}
