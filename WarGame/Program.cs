﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    class Program
    {
        const string PLAY = "play";
        const string EXIT = "exit";
        const string SHUFFLE = "shuffle";

        static void Main(string[] args)
        {
            CardDeck deck = new CardDeck();
            CardDeck PlayerDeck = new CardDeck();
            CardDeck OpponentDeck = new CardDeck();
            CardDeck WarPile = new CardDeck();

            Card PlayerCard = new Card();
            Card OpponentCard = new Card();

            try
            {
                //Fill the deck with cards (52 total) before starting the game
                deck.FillDeck();

                //Shuffle deck before dealing cards
                deck.Shuffle();

                //Deal first shuffled half to Player (26 total) 
                deck.Deal(ref PlayerDeck, 26);
                //Deal the rest to Opponent (26 total). 
                deck.Deal(ref OpponentDeck, 26);
                Console.WriteLine("Deck size: " + deck.GetCount());
    
                Console.Write("WAR has started! -> Press [ENTER] to start the game, press Escape [Esc] to EXIT >");
                bool ShuffleOption = false;
                string input = GetInput(ShuffleOption);
                
                bool StillPlaying = false; //For EXIT; if false, then game will exit. 
                if (input == PLAY)
                {
                    //Player always has 1st draw
                    PlayerCard = PlayerDeck.Draw();
                    Console.WriteLine("\nPLAYER drew a card! ->" + PlayerCard.ConvertToString());
                    OpponentCard = OpponentDeck.Draw();
                    Console.WriteLine("OPPONENT drew a card! ->" + OpponentCard.ConvertToString());
                    StillPlaying = true; //Set to true to enter the while loop
                    
                } else if (input == EXIT)
                {
                    Console.WriteLine("\nThank you for playing War!(a Card Game).");
                    //Game will exit automatically because StillPlaying is now false
                    StillPlaying = false;
                }
                bool AtWar = false; //to be true when at war; false by default
                while (PlayerCard != null && OpponentCard != null && StillPlaying)
                {
                    //Compare ranks and give both playing cards to owner with highest rank
                    if ((PlayerCard.CompareRank(OpponentCard)) > 0) //Player's rank is higher than opponent's
                    {
                        if (AtWar)
                        {
                            AtWar = false; //War has ended, so set back to false.
                            Console.WriteLine("\nPLAYER wins this war! ->Collect all cards");
                            //Collect all cards in the war pile
                            WarPile.Deal(ref PlayerDeck, WarPile.GetCount());
                            //Collect both cards at play
                            CollectCards(ref PlayerDeck, PlayerCard, OpponentCard);
                            //Give option to Player to shuffle after winning a war
                            if ( WantToShuffle() )
                            {
                                PlayerDeck.Shuffle();
                                Console.WriteLine(" Deck was shuffled!");
                            } else { Console.WriteLine(); } //enter line break if "shuffled" msg not outputted
                        }
                        else
                        {
                            //Regular round; no war
                            Console.WriteLine("\nPLAYER wins the round! ->Collect both cards");
                            CollectCards(ref PlayerDeck, PlayerCard, OpponentCard);
                        }
                    }
                    else if ((PlayerCard.CompareRank(OpponentCard)) < 0) //Player's rank is lower than opponent's
                    {
                        if (AtWar)
                        {
                            AtWar = false;
                            Console.WriteLine("\nOPPONENT wins this war! ->Collect all cards");
                            WarPile.Deal(ref OpponentDeck, WarPile.GetCount());
                            CollectCards(ref OpponentDeck, PlayerCard, OpponentCard);
                            //Opponent will always shuffle deck after winning war.
                            OpponentDeck.Shuffle();
                        }
                        else
                        {
                            Console.WriteLine("\nOPPONENT wins the round! ->Collect both cards");
                            CollectCards(ref OpponentDeck, PlayerCard, OpponentCard);
                        }
                    }
                    else //Both ranks are equal; WAR IS ON
                    {
                        if (AtWar) //If already at war, display different text
                        {
                            Console.WriteLine("Both ranks are equal again! ->War wages on");
                            Console.WriteLine("Both PLAYER and OPPONENT will each surrender another card!");
                            //If at war, then add playing cards to war pile 
                            CollectCards(ref WarPile, PlayerCard, OpponentCard);
                            //Also add a new card from each playing deck to war pile
                            PlayerDeck.Deal(ref WarPile, 1);
                            OpponentDeck.Deal(ref WarPile, 1);
                        }
                        else //Display different text on first round of war
                        {
                            AtWar = true;
                            Console.WriteLine("Both ranks are equal! ->War has been declared");
                            Console.WriteLine("Both PLAYER and OPPONENT will each surrender a card!");
                            CollectCards(ref WarPile, PlayerCard, OpponentCard);
                            PlayerDeck.Deal(ref WarPile, 1);
                            OpponentDeck.Deal(ref WarPile, 1);
                        }
                    }
                    Console.Write("Continue to next round? [Enter] or Exit [Esc] >");
                    ShuffleOption = false; //Shuffling is not a valid choice here 
                    input = GetInput(ShuffleOption);
                    if (input == PLAY)
                    {
                        //Start of next round
                        //Always clear the screen and project score (except when at war)
                        if ( !AtWar )
                        {
                            Console.Clear();
                            Console.WriteLine(PrintDeckScore(ref PlayerDeck, ref OpponentDeck));
                        }
                        PlayerCard = PlayerDeck.Draw();
                        if (PlayerCard != null)
                            Console.WriteLine("\nPLAYER drew a card! ->" + PlayerCard.ConvertToString());

                        OpponentCard = OpponentDeck.Draw();
                        if (OpponentCard != null)
                            Console.WriteLine("OPPONENT drew a card! ->" + OpponentCard.ConvertToString());
                    }
                    else if (input == EXIT)
                    {
                        Console.WriteLine("\nThank you for playing War!(a Card Game).");
                        //Game will exit automatically because StillPlaying is now false
                        StillPlaying = false;
                    }
                }//End of while

                //Output the winner
                if(PlayerCard == null) { Console.WriteLine("Opponent has won the game!"); }
                if(OpponentCard == null) { Console.WriteLine("Player has won the game!"); }

            } catch (IndexOutOfRangeException e)
            {
                Console.Error.WriteLine("Error: index is out of range: " + e.Message);
            } catch (ArgumentNullException e)
            {
                Console.Error.WriteLine("Error: Parameter: " + e.ParamName + " cannot be NULL: " + e.Message);
            } catch (ArgumentOutOfRangeException e)
            {
                Console.Error.WriteLine("Error: Argument is out of range: " + e.Message);
            }

            Console.WriteLine("**********Game has Ended**********");
        }

        /*****************************************************************
         * CollectCards  -Add 2 card to 1 deck; used for each round of the game
         * PARAMETER     -ref deck:CardDeck - deck to contain the 2 cards
         * PARAMETER     -card1:Card - 1st card to be added to deck
         * PARAMETER     -card2:Card - 2nd card to be added to deck
         * RETURN        -n/a
         ****************************************************************/
        static void CollectCards(ref CardDeck deck, Card card1, Card card2)
        {
            deck.AddCard(card1);
            deck.AddCard(card2);
        }

        /*****************************************************************
         * GetInput     -parses through user's input and returns a valid response
         * PARAMETER    -ShuffleOption:bool - turns on/off validation for the shuffle option
         * RETURN        -string - the user's response
         ****************************************************************/
        static string GetInput(bool ShuffleOption)
        {
            bool invalid = true;
            string input_string = null;

            while (invalid)
            {
                ConsoleKeyInfo input = Console.ReadKey();
                if (input.Key == ConsoleKey.Enter) { input_string = PLAY; invalid = false; }
                else if (input.Key == ConsoleKey.Escape) { input_string = EXIT; invalid = false; }
                else if ( (input.Key.ToString() == "S") && ShuffleOption ) { input_string = SHUFFLE; invalid = false; }
                else if ((input.Key.ToString() == "S") && !(ShuffleOption) )
                {
                    invalid = true;
                    Console.Error.WriteLine("Shuffling is not allowed at this time. Please use [Enter] or [Esc]");
                }
                else
                {
                    invalid = true;
                    Console.Error.WriteLine("--- Invalid input! Please use [ENTER] or [Esc]");
                }
            }
            return input_string;
        }

        /*****************************************************************
         * WantToShuffle -Gives user the option to shuffle
         * PARAMETER     -n/a
         * RETURN        -bool; if true, then the deck will be shuffled at the caller's section
         ****************************************************************/
        static bool WantToShuffle()
        {
            Console.Write("Would you like to shuffle your deck at this time? Press [s] to shuffle or [Enter] to continue > ");
            bool ShuffleOption = true;
            string response = GetInput(ShuffleOption);
            if ( response != SHUFFLE ) { ShuffleOption = false; }

            return ShuffleOption;
        }

        /*****************************************************************
         * PrintDeckScore   -Printout of score to be used as part of console output
         * PARAMETER        -ref player:CardDeck - Player's deck size
         * PARAMETER        -ref opponent:CardDeck - Opponent's deck size
         * RETURN           -string; contains a line with current score
         ****************************************************************/
        static string PrintDeckScore(ref CardDeck player, ref CardDeck opponent)
        {
            //Be careful you do NOT modify the decks here. You cannot pass a const ref to avoid this.
            return ("Player's deck (" + player.GetCount() + ") VS Opponent's deck (" + opponent.GetCount() + ')');
        }
    }
}
