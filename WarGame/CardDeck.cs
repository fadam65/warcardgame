﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    class CardDeck
    {
        private List<Card> deck;
        //Random seed for shuffle algorithm
        private Random randNum;

        /*****************************************************************
         * Constructor  -blank 
         *
         ****************************************************************/
        public CardDeck()
        {
         deck = new List<Card>();

         randNum = new Random();
        }

        /*****************************************************************
         * GetCount     -returns number of cards in deck
         * PARAMETER    -n/a
         * RETURN       -int; cound of deck 
         ****************************************************************/
        public int GetCount() { return deck.Count; }

        /*****************************************************************
         * FillDeck     -populates deck with the standard 52 playing cards
         * PARAMETER    -n/a
         * RETURN       -n/a
         ****************************************************************/
        public void FillDeck()
        {
            //Loop through the suits (1-4)
            for(int suit = 0; suit < 4; suit++)
            {
                for(int rank = 0; rank < 13; rank++)
                {
                    deck.Add(new Card(rank, suit));
                }
            }
            //Console.WriteLine("Length of Card deck is " + MainDeck.Count);
        }

        /*****************************************************************
         * PrintDeck   -outputs list of all cards in the deck
         * PARAMETER   -n/a
         * RETURN      -n/a
         ****************************************************************/
        public void PrintDeck()
        {
            foreach(Card card in deck)
            {
                Console.WriteLine(card.ConvertToString());
            }
        }

        /****************************************
         * Shuffle      -shuffles a deck using "Fisher-Yates shuffle"
         * PARAMETER    -n/a
         * RETURN       -n/a
         ***************************************/
        public void Shuffle()
        {
            int next = 0;
            Card card_temp = new Card();
            for(int i = 0; i < deck.Count; i++)
            {
                next = randNum.Next(i + 1);
                card_temp = deck[next];
                deck[next] = deck[i];
                deck[i] = card_temp;
            }
        }

        /*****************************************************************
         * Deal         -transfers specific amt of cards to another deck.
         * PARAMETER    -ref hand:CardDeck - the hand getting dealt from the deck
         * PARAMETER    -total_cards:int - cards to be tranferred
         * RETURN       -n/a
         ****************************************************************/
        public void Deal(ref CardDeck hand, int total_cards)
        {
            if (total_cards > deck.Count)
                throw new ArgumentOutOfRangeException(" Number of cards to deal (" + total_cards + ") is greater than size of deck (" + deck.Count + ')' );

            for (int i = 0; i < total_cards; i++)
            {
                hand.AddCard(deck[i]);
            }
            //Remove the cards just dealt from the original deck
            deck.RemoveRange(0, total_cards);
        }

        /*****************************************************************
         * AddCard      -Adds a card to the end of the deck(list)
         * PARAMETER    -NewCard:Card - card to add
         * RETURN       -n/a
         ****************************************************************/
        public void AddCard(Card NewCard)
        {
            if (NewCard.Equals(null))
                throw new ArgumentNullException("The argument cannot be NULL", "NewCard");
            else
                deck.Add(NewCard);
        }

        /*****************************************************************
         * Draw        -takes a card from the beginning of the deck(list)
         * PARAMETER   -n/a
         * RETURN      -Card; the card removed from the deck
         ****************************************************************/
        public Card Draw()
        {
            Card card = new Card();

            if (deck.Count == 0) { card = null; }
            else
            {
                //Get the first card in the list and remove it from deck
                card = deck[0];
                deck.RemoveAt(0);
            }

            return card;
        }
    }
}
